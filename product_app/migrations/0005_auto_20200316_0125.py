# Generated by Django 3.0.4 on 2020-03-16 01:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product_app', '0004_auto_20200316_0124'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productlist',
            name='product_id',
        ),
        migrations.AddField(
            model_name='productlist',
            name='product_id',
            field=models.ManyToManyField(to='product_app.Product'),
        ),
    ]
