# Generated by Django 3.0.4 on 2020-03-16 01:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('product_app', '0003_auto_20200316_0117'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productlist',
            name='product_id',
        ),
        migrations.AddField(
            model_name='productlist',
            name='product_id',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='product_app.Product'),
            preserve_default=False,
        ),
    ]
