from django.db import models

from client_app.models import Client


# Create your models here.

class Product(models.Model):
    """ This model is for client product.  """
    # #id do produto p/ aceitar 1bf0f365-fbdd-4e21-9786-da459d78dd1f
    id = models.CharField(primary_key=True, editable=False, max_length=100)
    #nome do produto
    title = models.CharField(max_length = 150)
    #preço do produto 
    prince = models.FloatField() 
    #url da imagem do produto
    image = models.CharField(max_length=300) 
    #marca do produto
    brand = models.CharField(max_length = 150)
    #média dos reviews para este produto
    reviewScore = models.FloatField()

    def __str__(self):
        return self.title

    
class ProductList(models.Model):
    """ This model is for product list.  """
    #apenas um cliente para cada lista 
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    #varios produtos para uma lista
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    

    def __str__(self):
        return '%s - %s - %s'%(self.client_id, self.client.name, self.product)


    class Meta:
        unique_together = ('client', 'product') #restrincao| esses dois nunca poderao cair juntos   
