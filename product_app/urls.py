from django.urls import path

from django.conf import settings

from django.conf.urls.static import static

from product_app.views import ProductCreate, ProductUpdate, ProductList, ProductDelete


urlpatterns = [
    
    path('create/', ProductCreate.as_view(), name='product_create'),

    path('update/', ProductUpdate.as_view(), name='product_update'),

    path('list/', ProductList.as_view(), name='product_list'),

    path('delete/', ProductDelete.as_view(), name='product_delete'),



]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)