
from django.shortcuts import render

from product_app.models import Product

from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django.views.generic import ListView



class ProductCreate(CreateView):
    model = Product
    fields = ['title']


class ProductUpdate(UpdateView):
    model = Product
    fields = ['title']
    template_name_suffix = '_update_form'


class ProductList(ListView):
    model = Product


class ProductDelete(DeleteView):
    model = Product
    # success_url = reverse_lazy('Product-list') 