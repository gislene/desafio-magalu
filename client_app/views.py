from django.shortcuts import render

from django.urls import reverse_lazy    

from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django.views.generic import ListView

from client_app.models import Client


class ClientCreate(CreateView):
    model = Client
    fields = ['name']


class ClientUpdate(UpdateView):
    model = Client
    fields = ['name']
    template_name_suffix = '_update_form'


class ClientList(ListView):
    model = Client
    # context_object_name = 'my_favorite_publishers' 

class ClientDelete(DeleteView):
    model = Client
    # success_url = reverse_lazy('client-list') 