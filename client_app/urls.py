from django.urls import path

from client_app.views import ClientCreate, ClientUpdate, ClientList, ClientDelete


urlpatterns = [

    path('create/', ClientCreate.as_view(), name='client_create'),

    path('update/', ClientUpdate.as_view(), name='client_update'),

    path('list/', ClientList.as_view(), name='client_list'),

    path('delete/', ClientDelete.as_view(), name='client_delete'),


]