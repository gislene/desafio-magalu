from django.db import models

# Create your models here.

class Client(models.Model):
    """ This model is for client create.  """
    #nome do cliente 
    name = models.CharField(max_length=254)
    #e-mail do cliente
    email = models.EmailField(unique=True, max_length = 254) 

    def __str__(self):
        return self.name

        