from django.contrib import admin

# Register your models here.

from client_app.models import Client

from product_app.models import Product, ProductList

admin.site.register(Client)

admin.site.register(Product)

admin.site.register(ProductList)
